# Python Starter &nbsp; <img height="45px" style="margin-bottom: -10px" src=https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg> 

## Tooling
### Base
- Package Manager: **pipenv**
- Type Checking: **mypy**
- Formatting: **black**
- Linting: **pylint/bandit**
- Package Security: **safety**
- Commit Formatter: **git-cz**
- Logging: **logging** 
- Error Monitoring: **sentry** 
- Data Validation: **pydantic**

## Monorepo Layout
- **clients/**  
  - Data Sources: sql, s3, graphql, rest api/json schema
- **domain/** 
  - Biz logic
- **ui/** 
  - User Interfaces (in the purest sense of the term):  Can be graphql, rest api, or cli

```
├── clients
│   ├── clients
│   │   └── __init__.py
│   ├── Pipfile
│   └── setup.py
├── domain
│   ├── domain
│   │   ├── __init__.py
│   │   └── utils.py
│   ├── Pipfile
│   └── setup.py
├── mypy.ini
├── .pylintrc
├── readme.md
└── ui
    └── cli
        ├── Pipfile
        ├── Pipfile.lock
        └── src
            └── main.py
```