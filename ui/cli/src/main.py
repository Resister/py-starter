import logging
import sentry_sdk

from clients import clients_fn
from domain import domain_fn, utils


def main() -> None:
    sentry_sdk.init(utils.config.sentry_url)
    utils.setup_logger(utils.config)
    logging.info("CLI is starting...")
    logging.info(domain_fn())
    logging.info(clients_fn())


main()
