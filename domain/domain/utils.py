import logging
from os import environ
from typing import Type, Union, Optional, Literal
from distutils.util import strtobool

from pydantic import BaseModel, HttpUrl

Parseable = Union[str, bool, int]


def _parse(key: str, type_arg: Type[Parseable]) -> Parseable:
    if type_arg == int:
        return int(key)
    if type_arg == bool:
        return bool(strtobool(key))
    if type_arg == str:
        return key
    raise ValueError("Can only parse boolean, number and strings")


def get_var(
    key: str,
    default: Optional[Parseable] = None,
    type_arg: Optional[Type[Parseable]] = None,
) -> Parseable:
    res = environ.get(key)
    if res and default:
        return _parse(res, type(default))
    if res and type_arg:
        return _parse(res, type_arg)
    if not res and default:
        return default
    if not default or not type_arg:
        raise ValueError("Either type_arg or default required")
    raise ValueError(f"Environment Variable {key} Required")


class Config(BaseModel):
    log_level: Literal["ERROR", "WARN", "INFO", "DEBUG"]
    sentry_url: HttpUrl


config = Config(
    sentry_url=get_var("SENTRY_URL", type_arg=str),
    log_level=get_var("LOG_LEVEL", "DEBUG"),
)


def setup_logger(cfg: Config) -> None:
    level = cfg.log_level.upper()
    # https://github.com/python/mypy/issues/11251
    # match level:
    #     case "ERROR" | "WARN" | "INFO" | "DEBUG":
    #         pass
    #     case _:
    #         raise ValueError("Logger only accepts ERROR, WARN, INFO, and DEBUG levels")
    logging.basicConfig(format="%(levelname)s: %(message)s", level=level)
